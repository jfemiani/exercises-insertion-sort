.. role:: c(code)
	:language: c

Insertion Sort
--------------
:points: 10
:time: 1-2 hours
:source: https://bitbucket.org/ser222/exercises-insertion-sort.git

Your job is to write an program that reads a list of strings from STDIN
until you encounter the end of input, and then to print the strings out in
sorted order using the *insertion sort* algorithm. You will receive a
maximum of 100 strings, and no string will be more than 32 characters
including the null character. In order to keep this assignment simple
you may assume input has been entered without user-error (no need to
validate input).

I have provided a file named file:./insert_sort.c below.

Replace the :c:`//TODO` comments so that the output is sorted.

.. include:: insert_sort.c
    	:code: c
	:number-lines:


You can compile and test your code in one step by running the file:./test script.
When you pass all the tests, make sure you have a working internet connection and
submit it with the file:./submit script.

.. code:: bash

	$ ./test && ./submit

Make sure to test it before calling submit!  Assignments that do not pass the tests 
will not be graded at all (they will receive a zero if not submitted on time)


